import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import Main from './components/HomeComponent';
import Menu from './components/MenuComponent';
import {Header} from './components/HeaderComponent';
class App extends Component {
  
  render() {
    const arr = [{id: 1, name: 'Join'},{id:2, name:'Order food'}];

    const menuItems = [{id: 1, name: 'Manila'},{id:2, name:'Cebu'}];


    // const arr = [1,2,3];
    return (
  
      <div className="grid-container">
        <div className="item1"> <Header users={arr} /></div>
        <div className="item2"><Menu  menuItems={menuItems}  /></div>
        <div className="item3"> <Main /></div>  
        <div className="item4">Right</div>
        <div className="item5">Footer</div>
    </div>      
    );
  }
}

export default App;

