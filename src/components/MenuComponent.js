import React from 'react';
const Menu = (props)=> {
  const Display = () => {
    return props.menuItems.map(m => (<li>{m.name}</li>));
  }
  return (
    <div>
      <h1 >Find us in your city</h1>
      <h3><Display  /></h3>
    </div>); 
}

export default Menu;